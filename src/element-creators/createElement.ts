import { addToFavs } from '../functions/addToFavs';

type cardType = 'fav' | 'main';

export function createElement(type: cardType, movieId: number, moviePoster: string, movieDesc: string) {
    const maindiv: HTMLDivElement = document.createElement("div");
    if (type === 'main') {
      maindiv.setAttribute("class", "col-lg-3 col-md-4 col-12 p-2");
    }
    if (type === 'fav') {
      maindiv.setAttribute("class", "col-12 p-2");
    }
    
    const cardDiv: HTMLDivElement = document.createElement("div");
    cardDiv.setAttribute("class", "card shadow-sm");

    const posterImg: HTMLImageElement = document.createElement("img");
    posterImg.setAttribute("src", `https://image.tmdb.org/t/p/original/${moviePoster}`) // custom img
    posterImg.setAttribute("alt", 'movie poster')
    
    const isFaved: string | null = window.localStorage.getItem(movieId.toString());

    const iconSVG: SVGSVGElement =  document.createElementNS("http://www.w3.org/2000/svg", "svg");
    iconSVG.setAttribute("stroke", "red");

    if (type === 'main') {
      if (isFaved) {
        iconSVG.setAttribute("fill", "red");
      } else {
        iconSVG.setAttribute("fill", "#ff000078");
      }
    }
    if (type === 'fav') {
      iconSVG.setAttribute("fill", "red");
    }

    iconSVG.setAttribute("width", "50");
    iconSVG.setAttribute("height", "50");
    iconSVG.setAttribute("class", "bi bi-heart-fill position-absolute p-2");
    iconSVG.setAttribute("viewBox", "0 -2 18 22");
    iconSVG.setAttribute("id", `${movieId}`);

    const pathSVG: SVGPathElement =  document.createElementNS("http://www.w3.org/2000/svg", "path");
    pathSVG.setAttribute("fill-rule", "evenodd");
    pathSVG.setAttribute("d", "M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z");
    iconSVG.addEventListener("click", function(this: HTMLElement):void {addToFavs(this)}, true);
    iconSVG.append(pathSVG);

    const cardBodyDiv: HTMLDivElement = document.createElement("div");
    cardBodyDiv.setAttribute("class", "card-body");

    const cardBodyDivP: HTMLParagraphElement = document.createElement("p");
    cardBodyDivP.setAttribute("class", "card-text truncate");
    cardBodyDivP.innerText = `${movieDesc}`; // custom movie desc

    const dateDiv: HTMLDivElement = document.createElement("div");
    dateDiv.setAttribute("class", `d-flex
                                   justify-content-between
                                   align-items-center`
    );
    const dateDivSmall: HTMLElement = document.createElement("small");
    dateDivSmall.setAttribute("class", "text-muted");
    dateDivSmall.innerText = "2021-05-26";

    dateDiv.append(dateDivSmall);

    cardBodyDiv.append(cardBodyDivP, dateDiv);

    cardDiv.append(posterImg, iconSVG, cardBodyDiv);

    maindiv.append(cardDiv);
    
    return maindiv;
  }

  