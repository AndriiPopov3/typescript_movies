export function createLoadMoreButton(): HTMLButtonElement {
    const loadMoreButton: HTMLButtonElement = document.createElement("button");
    loadMoreButton.setAttribute("id", "load-more");
    loadMoreButton.setAttribute("type", "button");
    loadMoreButton.setAttribute("class", "btn btn-lg btn-outline-success");
    loadMoreButton.innerText = "Load more";
    return loadMoreButton;
  }
