import { createElement } from '../element-creators/createElement';
import { env_values} from '../api.config/api.access';

export function updateFavContainer():void {
    const favoriteContainer: HTMLElement | null = document.getElementById("favorite-movies");
    while(favoriteContainer?.firstChild) {
        favoriteContainer?.removeChild(favoriteContainer?.firstChild);
    }
    let x: any; // server response is held here;
    if (favoriteContainer) {
        for (let i: number = 0; i < window.localStorage.length; i++) {
            let xhr2 = new XMLHttpRequest();
            xhr2.open('GET', `${env_values.DEFAULT_URL}movie/${Object.keys(window.localStorage)[i]}?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}`, true);
            xhr2.onload = function ():void {
                x = JSON.parse(xhr2.response);
                let favMovieCard: HTMLElement;
                favMovieCard = createElement('fav', x.id, x.poster_path, x.overview);
                favoriteContainer.append(favMovieCard);
            };
            xhr2.send();
        }
    }
  }

  