import { Movie } from '../helpers/mapper';

export function setRandomMovie(results: Movie[]):void {
    const randomMovie:HTMLElement | null = document.getElementById("random-movie");
    const randomMovieName:HTMLElement | null = document.getElementById("random-movie-name");
    const randomMovieDesc:HTMLElement | null = document.getElementById("random-movie-description");
    let randomMovieNum:number = Math.floor(Math.random() * (20 - 0)) + 0;

    if(randomMovie) {
        randomMovie.setAttribute("style", `background-image: url('https://image.tmdb.org/t/p/original/${results[randomMovieNum].backdrop_path}');background-size: cover;`);
    }
    if(randomMovieName) {
        randomMovieName.innerText = `${results[randomMovieNum].title}`;
    }
    if(randomMovieDesc) {
        randomMovieDesc.innerText = `${results[randomMovieNum].overview}`;
    }
  }

  