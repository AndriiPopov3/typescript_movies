import { createElement } from '../element-creators/createElement';
import { Movie } from '../helpers/mapper';

export function updateFilmContainer(result: Movie[]):void {
    const filmContainer: HTMLElement | null = document.getElementById("film-container");
    let movieCard: HTMLElement;
    if(filmContainer) {
        filmContainer.innerHTML = "";
        for(let i:number = 0; i < result.length; i++) {
            movieCard = createElement('main', result[i].id, result[i].poster_path, result[i].overview);
            filmContainer.append(movieCard);
        }
    }
}

  