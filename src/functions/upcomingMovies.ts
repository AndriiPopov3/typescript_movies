import { updateFilmContainer } from '../updaters/updateFilmContainer';
import { setRandomMovie } from '../updaters/randomMovie';
import { modifyLoadMoreButton } from './loadMore';
import { mapper, Movie } from '../helpers/mapper';
import { modifyFavContainer } from './addToFavs';
import { env_values} from '../api.config/api.access';

export function modifyUpcomingButton():void {
  const upcomingButton: HTMLElement | null = document.getElementById("upcoming");
  upcomingButton?.addEventListener('click', () => upcomingMovies(), false);
}

export function upcomingMovies():void {
  modifyLoadMoreButton("upcoming");
  modifyFavContainer();
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `${env_values.DEFAULT_URL}movie/upcoming?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&page=1`, true);
  let x: any;
  xhr.onload = function () {
    x = JSON.parse(xhr.response);
    const result: Movie[] = mapper(x.results);
    setRandomMovie(result);
    updateFilmContainer(result);
  };
  xhr.send();
}
  