import { createElement } from '../element-creators/createElement';
import { createLoadMoreButton } from '../element-creators/createLoadMoreButton';
import { mapper, Movie } from '../helpers/mapper';
import { env_values} from '../api.config/api.access';

export function modifyLoadMoreButton(mode: string):void {
  const loadMoreDiv: HTMLElement | null = document.getElementById("load-more-div");
  while(loadMoreDiv?.firstChild) {
    loadMoreDiv?.removeChild(loadMoreDiv?.firstChild);
  }
  const loadMoreButton: HTMLButtonElement = createLoadMoreButton();
  loadMoreButton.addEventListener('click', () => loadMoreMovies(mode), false);
  loadMoreDiv?.append(loadMoreButton);
}

const loadMoreMovies = function(mode: string):void {
  const filmContainer:HTMLElement | null = document.getElementById("film-container");
  const cardNum: number = document.getElementsByClassName("card").length;
  const pageNum: number = Math.floor((cardNum - window.localStorage.length) / 20) + 1;  // resulting array includes cards in fav-container so they are subtracted to get an accurate current number of cards on page
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `${env_values.DEFAULT_URL}movie/${mode}?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&page=${pageNum}`, true);
  let x: any; // server response is held here
  xhr.onload = function () {
    x = JSON.parse(xhr.response);
    if (x) {
      const result: Movie[] = mapper(x.results);
      let movieCard: HTMLElement;
      if(filmContainer) {
        for (let i:number = 0; i < result.length; i++) {
          movieCard = createElement('main', result[i].id, result[i].poster_path, result[i].overview);
          filmContainer.append(movieCard);
        }
      }
    }
  };
  xhr.send();
}

export function modifyLoadMoreButtonSearch(query: string):void {
  const loadMoreDiv:HTMLElement | null = document.getElementById("load-more-div");
  while(loadMoreDiv?.firstChild) {
    loadMoreDiv?.removeChild(loadMoreDiv?.firstChild);
}
  const loadMoreButton: HTMLButtonElement = createLoadMoreButton();
  loadMoreButton.addEventListener('click', () => loadMoreMoviesSearch(query), false);
  loadMoreDiv?.append(loadMoreButton);
}

export const loadMoreMoviesSearch = function(query: string):void {
  const filmContainer:HTMLElement | null = document.getElementById("film-container");
  const cardNum: number = document.getElementsByClassName("card").length;
  const pageNum: number = Math.floor((cardNum - window.localStorage.length) / 20) + 1;
  const xhr = new XMLHttpRequest();
  if ((cardNum - window.localStorage.length) % 20 === 0) { // if not 0 this is the last page of results for the search query
    const default_url: string = `${env_values.DEFAULT_URL}search/movie?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&query=${query}&page=${pageNum}&include_adult=false`;
    const encoded: string = encodeURI(default_url);
    xhr.open('GET', encoded, true);
    let x: any; // server response is held here
    xhr.onload = function () {
      x = JSON.parse(xhr.response);
      if (x) {
        const result: Movie[] = mapper(x.results);
        let movieCard: HTMLElement;
        if(filmContainer) {
        for (let i = 0; i < result.length; i++) {
          movieCard = createElement('main', result[i].id, result[i].poster_path, result[i].overview);
            filmContainer.append(movieCard);
        }
      }
      }
    };
    xhr.send();
  }
}