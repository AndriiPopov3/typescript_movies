import { createElement } from '../element-creators/createElement';
import { popularMovies } from './popularMovies';
import { upcomingMovies } from './upcomingMovies';
import { topRatedMovies } from './topRatedMovies';
import { mapper, Movie } from '../helpers/mapper';
import { modifyLoadMoreButtonSearch } from './loadMore';
import { env_values} from '../api.config/api.access';

export function modifyFormSubmit():void {
    const searchButton = <HTMLButtonElement>document.getElementById("submit");
    searchButton?.addEventListener('click', () => searchMovies(), false);
}

function searchMovies():void {
    const filmContainer: HTMLElement | null = document.getElementById("film-container");
    const inputSearch = <HTMLInputElement>document.getElementById('search');
    const searchValue: string = inputSearch?.value;
    if(searchValue) {
    modifyLoadMoreButtonSearch(searchValue);
    const xhr = new XMLHttpRequest();
    const default_url: string = `${env_values.DEFAULT_URL}search/movie?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&query=${searchValue}&page=1&include_adult=false`;
    const encoded: string = encodeURI(default_url);
    xhr.open('GET', encoded, true);
    let x: any; // server response
    xhr.onload = function ():void {
        x = JSON.parse(xhr.response);
        const result: Movie[] = mapper(x.results);
        let movieCard: HTMLElement;
            if(filmContainer) {
              filmContainer.innerHTML = "";
                for(let i: number = 0; i < result.length; i++) {
                    movieCard = createElement('main', result[i].id, result[i].poster_path, result[i].overview);
                    filmContainer.append(movieCard);
                }
            }
    };
    xhr.send();
    } else {
        const popularInput = <HTMLInputElement>document.getElementById('popular');
        const top_ratedInput = <HTMLInputElement>document.getElementById('top_rated');
        const upcomingInput = <HTMLInputElement>document.getElementById('upcoming');
        if(popularInput?.checked) {
            popularMovies();
        }
        if(top_ratedInput?.checked) {
            topRatedMovies();
        }
        if(upcomingInput?.checked) {
            upcomingMovies();
        }
    }
}
