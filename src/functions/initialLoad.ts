import { setRandomMovie } from '../updaters/randomMovie';
import { mapper, Movie } from '../helpers/mapper';
import { updateFavContainer } from '../updaters/updateFavContainer';
import { updateFilmContainer } from '../updaters/updateFilmContainer';
import { env_values} from '../api.config/api.access';

export function initialLoad():void {
    const xhr = new XMLHttpRequest();
    xhr.open('GET',
             `${env_values.DEFAULT_URL}movie/popular?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&page=1`,
             true);
    let x: any; // server response is held here
    xhr.onload = function ():void {
        x = JSON.parse(xhr.response);
        const result: Movie[] = mapper(x.results);
        setRandomMovie(result);
        updateFilmContainer(result);
    };
    xhr.send();
    updateFavContainer();
}