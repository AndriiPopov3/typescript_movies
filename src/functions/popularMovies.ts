import { setRandomMovie } from '../updaters/randomMovie';
import { modifyLoadMoreButton } from './loadMore';
import { updateFilmContainer } from '../updaters/updateFilmContainer';
import { mapper, Movie } from '../helpers/mapper';
import { modifyFavContainer } from './addToFavs';
import { env_values} from '../api.config/api.access';

export function modifyPopularButton():void {
  const upcomingButton: HTMLElement | null = document.getElementById("popular");
  upcomingButton?.addEventListener('click', () => popularMovies(), false);
}

export function popularMovies():void {
  modifyLoadMoreButton("popular");
  modifyFavContainer();
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `${env_values.DEFAULT_URL}movie/popular?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&page=1`, true);
  let x: any;
  xhr.onload = function ():void {
    x = JSON.parse(xhr.response);
    const result: Movie[] = mapper(x.results);
    setRandomMovie(result);
    updateFilmContainer(result);
  };
  xhr.send();
}
