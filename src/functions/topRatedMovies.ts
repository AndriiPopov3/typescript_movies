import { updateFilmContainer } from '../updaters/updateFilmContainer';
import { setRandomMovie } from '../updaters/randomMovie';
import { modifyLoadMoreButton } from './loadMore';
import { mapper, Movie } from '../helpers/mapper';
import { modifyFavContainer } from './addToFavs';
import { env_values} from '../api.config/api.access';

export function modifyTopRatedButton():void {
  const upcomingButton: HTMLElement | null = document.getElementById("top_rated");
  upcomingButton?.addEventListener('click', () => topRatedMovies(), false);
}

export function topRatedMovies():void {
  modifyLoadMoreButton("top_rated");
  modifyFavContainer();
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `${env_values.DEFAULT_URL}movie/top_rated?api_key=${env_values.API_TOKEN}${env_values.DEFAULT_LANG}&page=1`, true);
  let x;
  xhr.onload = function ():void {
    x = JSON.parse(xhr.response);
    if(x) {
      const result: Movie[] = mapper(x.results);
      setRandomMovie(result);
      updateFilmContainer(result);
    }
  };
  xhr.send();
}
