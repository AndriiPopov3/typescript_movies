import { updateFavContainer } from '../updaters/updateFavContainer';
import { popularMovies } from './popularMovies';
import { upcomingMovies } from './upcomingMovies';
import { topRatedMovies } from './topRatedMovies';

export function addToFavs(movie: HTMLElement):void {
    const myStorage: Storage = window.localStorage;
    const isMovieLiked: string | null = myStorage.getItem(movie.id);
    if (isMovieLiked) {
        myStorage.removeItem(movie.id);
        movie.setAttribute("fill", "#ff000078");
        updateFavContainer();
    } else {
        myStorage.setItem(movie.id, movie.id);
        movie.setAttribute("fill", "red");
        updateFavContainer();
    }
  }

export function modifyFavContainer():void {
    const favoriteContainer: HTMLElement | null = document.getElementById("favorite-movies");
    favoriteContainer?.setAttribute("tabindex", "0");
    const popularInput = <HTMLInputElement>document.getElementById('popular');
    const top_ratedInput = <HTMLInputElement>document.getElementById('top_rated');
    const upcomingInput = <HTMLInputElement>document.getElementById('upcoming');
    favoriteContainer?.removeEventListener("blur", popularMovies, true);
    favoriteContainer?.removeEventListener("blur", topRatedMovies, true);
    favoriteContainer?.removeEventListener("blur", upcomingMovies, true);
    if(popularInput?.checked) {
        favoriteContainer?.addEventListener("blur", popularMovies, true);
    }
    if(top_ratedInput?.checked) {
        favoriteContainer?.addEventListener("blur", topRatedMovies, true);
    }
    if(upcomingInput?.checked) {
        favoriteContainer?.addEventListener("blur", upcomingMovies, true);
    }
}