import { initialLoad } from './functions/initialLoad';
import { modifyPopularButton } from './functions/popularMovies';
import { modifyUpcomingButton } from './functions/upcomingMovies';
import { modifyTopRatedButton } from './functions/topRatedMovies';
import { modifyLoadMoreButton } from './functions/loadMore';
import { modifyFormSubmit } from './functions/searchMovie';
import { modifyFavContainer } from './functions/addToFavs';

export async function render(): Promise<void> {
    // TODO render your app here
    initialLoad();
    modifyUpcomingButton();
    modifyTopRatedButton();
    modifyPopularButton();
    modifyLoadMoreButton("popular");
    modifyFormSubmit();
    modifyFavContainer();
}
