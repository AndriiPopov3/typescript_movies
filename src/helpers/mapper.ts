export interface Movie {
    id: number;
    backdrop_path: string;
    title: string;
    overview: string;
    poster_path: string;
}

export function mapper(result: any[]):Movie[] {
    let mapped_result: Movie[] = [];
    for (let i: number = 0; i < result.length; i++) {
        let movie: Movie = {
            id: result[i].id,
            backdrop_path: result[i].backdrop_path,
            title: result[i].title,
            overview: result[i].overview,
            poster_path: result[i].poster_path
        }
        mapped_result.push(movie);
    }
    return mapped_result;
}